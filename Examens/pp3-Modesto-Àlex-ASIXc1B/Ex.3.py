"""

    Àlex Modesto Mena
    ASIXc 1B
    01/02/2022
    ENCRIPTAR FRASES
    Feu un programa per llegir una frase pel teclat i, en acabar, mostri la frase encriptada. Per encriptar la frase, ha
    de fer servir la posició de cadascuna de les lletres que haruan d'estar emmagatzemades. Els espais en blanc no s'han
    de codificar i entre el codi numèric de cada lletra, caldrà escriure el caràcter: per poder identificar a on comença
    i acaba cada lletra. No fer diferències entre majúscules i minúscules.

"""

un = ["a", "á", "à", "ä"]
dos = ["b"]
tres = ["c"]
quatre = ["d"]
cinc = ["e", "é", "è", "ë"]
sis = ["f"]
set = ["g"]
vuit =["h"]
nou = ["i", "í", "ì", "ï"]
deu = ["j"]
onze = ["k"]
dotze = ["l"]
tretze = ["m"]
catorze = ["n"]
quinze = ["o", "ó", "ò", "ö"]
setze = ["p"]
disset = ["q"]
divuit = ["r"]
dinou = ["s"]
vint = ["t"]
vint_i_un = ["u", "ú", "ù", "ü"]
vint_i_dos = ["v"]
vint_i_tres = ["w"]
vint_i_quatre = ["x"]
vint_i_cinc = ["y"]
vint_i_sis = ["z"]

frase = input("Frase? ")

for lletra in frase:
    if lletra in un:
        print("1", end="")
    elif lletra in dos:
        print("2", end="")
    elif lletra in tres:
        print("3", end="")
    elif lletra in quatre:
        print("4", end="")
    elif lletra in cinc:
        print("5", end="")
    elif lletra in sis:
        print("6", end="")
    elif lletra in set:
        print("7", end="")
    elif lletra in vuit:
        print("8", end="")
    elif lletra in nou:
        print("9", end="")
    elif lletra in deu:
        print("10", end="")
    elif lletra in onze:
        print("11", end="")
    elif lletra in dotze:
        print("12", end="")
    elif lletra in tretze:
        print("13", end="")
    elif lletra in catorze:
        print("14", end="")
    elif lletra in quinze:
        print("15", end="")
    elif lletra in setze:
        print("16", end="")
    elif lletra in disset:
        print("17", end="")
    elif lletra in divuit:
        print("18", end="")
    elif lletra in dinou:
        print("19", end="")
    elif lletra in vint:
        print("20", end="")
    elif lletra in vint_i_un:
        print("21", end="")
    elif lletra in vint_i_dos:
        print("22", end="")
    elif lletra in vint_i_tres:
        print("23", end="")
    elif lletra in vint_i_quatre:
        print("24", end="")
    elif lletra in vint_i_cinc:
        print("25", end="")
    elif lletra in vint_i_sis:
        print("26", end="")
    else:
        print(lletra, end="")


print("\nPROGRAMA FINALIZAT!")



