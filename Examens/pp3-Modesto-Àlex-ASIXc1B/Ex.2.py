"""

    Àlex Modesto Mena
    ASIXc 1B
    01/02/2022
    VORES DE LA MATRIU
    Es demana el nombre de files i columnes d'una matriu. COmprovar que la matriu sigui quadra, és a dir que tingui les
    mateixes fileres que columnes. Omplir la matriu amb 1's a les vores i la resta amb 0's. Mostra la matriu resultant.
    S'enten per vores de la matriu la primera i última filera i la primera i última columna.

"""

columna = int(input("\nIntrodueix el número de columnes (en NUMEROS!!!): "))
fila = int(input("Introdueix el número de files (en NUMEROS!!!): "))

while columna != fila or columna % 2 == 0 and fila % 2 == 0:
    print("\nEl número de files i columnes ha ser el mateix!!")
    columna = int(input("\nIntrodueix el número de columnes (en NUMEROS!!!): "))
    fila = int(input("Introdueix el número de files (en NUMEROS!!!): "))

print("Aqui tens la teva taula!!")
print()
for y in range(columna):
    for x in range(fila):
        if y == 0 or y == columna -1:
            print(" 1 ", end="")
        elif x == 0 or x == fila - 1:
            print(" 1 ", end="")
        else:
            print(" 0 ", end="")

    print("")

print("\nPROGRAMA FINALITZAT")
