"""

    Àlex Modesto Mena

    14/12/2021

    ASIXc1B M03 UF1 Examen

    E1.L'usuari introduirà una llista d'enters. Prèviament dirà quants enters introduirà. Un cop llegits tots, mostrar
    per pantalla la suma de tots els valors positius.

"""

numEnters = int(input("Quants enters introduiras? "))
suma = 0

for i in range(numEnters):
    numeros = input("Numeros? ")
    if int(numeros) >= 0:
        suma = suma + int(numeros)
print(f"\nLa suma dels valors positius introduïts és {suma}")

print("\nPrograma Finalitzat")

