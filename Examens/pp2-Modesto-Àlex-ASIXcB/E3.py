"""

    Àlex Modesto Mena

    14/12/2021

    ASIXc1B M03 UF1 Examen

    E3.Programa que pinta per pantalla un taulell d'escacs marcat amb una B les caselles blanques, i amb una N les negres.
    El programa, haurà de marcar amb * les caselles a les quals pots moure's una torre des d'una posició concreta que cal demanar a l'usuari per al terminal.
    Un taulell d'escats, és una matriu de 8x8.
    És a dir, 8 files per 8 columnes.
    La torre és una figura que només permet moviments en horitzontal i en vertical.

"""

#Variables
BLANC = " B "
NEGRE = " N "
TORRE = " * "
TAULELL = 8 #El taulell haura de ser sempre de 8x8

#Intro
print("Que comenci el joc!!!")

#Demanar les dades a l'usuari
columnes = float(input("\nQuina Columna vols? "))
files = float(input("I quina fila vols? "))

#Part de "calcul"
for x in range (TAULELL):
    for i in range (TAULELL):
        if x == files or i == columnes:
            print(TORRE, end="")
        elif (x + i) %2==0:
            print(BLANC, end="")
        else:
            print(NEGRE,end="")
        print()
#El progrma no funciona del tot
