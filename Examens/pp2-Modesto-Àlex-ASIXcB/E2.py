"""

    Àlex Modesto Mena

    14/12/2021

    ASIXc1B M03 UF1 Examen

    E2.Programa que implementi la funcionalitat del mètode replace. (sense fer servir el mètode)
    És a dir un programa que demani una cadena de caràcters, un caràcter a modificar i un altre caràcter de substitució.
    El programa haurà de mostrar com a resultat la cadena amb tots els caràcters a modificar canviats pel caràcter de substitució.

"""

frase = input("Introdueix una frase? ")
caracterA = input("Quin caràcter vols replaçar? ")
caracterB = input("Quin és el caràcter de substitució? ")

for x in frase:
    if x == caracterA:
        x = caracterB
    print(x,end="")

print("Programa Finalitzat")
