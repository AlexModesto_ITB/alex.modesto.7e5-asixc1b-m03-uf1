"""

   Alex Modesto Mena

   26/10/2021

   ASIXc1B M03 UF1 A2

   Realitza un programa que demani un nom d'usuari i una contrasenya. Si l'usuari és "pepe" i la contrasenya "asdasd",
   el programa mostrarà el missatge "Has entrat a el sistema". En cas contrari es mostrarà un missatge d'error (com per
   exemple, "Usuari / password incorrecte")

"""

usuari = input("Usuari? ")
contrasenya = input("Contrasenya? ")

if usuari == "pepe" and contrasenya == "asdasd":
    print("\nIniciant sessió:")
elif usuari == "pepe" and contrasenya != "asdasd":
    print("\nError en la contrasenya")
elif usuari != "pepe" and contrasenya == "asdasd":
    print("\nError en l'usuari")
else:
    print("\nUsuari i contrasenya incorrectes")
