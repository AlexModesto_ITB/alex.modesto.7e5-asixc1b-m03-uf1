"""

   Àlex Modesto Mena

   27/10/2021

   ASIXc1B M03 UF1 A2

   Un grup d'investigadors ens ha demanat un programa per generar retrats robots.
   L'usuari ha d'introduir el tipus de cabells, ulls, nas i boca i s'imprimeix per pantalla un dibuix de com és el sospitós.
   Els cabells poden ser arrissats @@@@@, llisos VVVVV o pentinats XXXXX
   Els ulls poden ser aclucats .-.-., rodons .o-o. o estrellats .*-*.
   El nas pot ser aixafat ..0.., arromangat ..C.. o agilenc ..V..
   La boca pot ser normal .===. , bigoti  .∼∼∼.  o dents-sortides  .www.

"""
#CABELLS
CABELL_ARRISSAT = "@@@@@"
CABELL_LLIS = "VVVVV"
CABELL_PENTINAT = "XXXXX"

#ULLS
ULLS_ACLUCATS = ".-.-."
ULLS_RODONS = ".O-O."
ULLS_ESTRELLATS = ".*-*."

#NAS
NAS_AIXAFAT = "..0.."
NAS_ARROMANGAT = "..C.."
NAS_AGILENC = "..V.."

#BOCA
BOCA_NORMAL = "==="
BOCA_BIGOTI = "∼∼∼"
BOCA_DENTS_SORTIDES = ".WWW."

print("Dels cabells tens les següents opcions: arrissats, llisos o pentinats")
cabell = input("Quin cabell té? ")
print("\nDels ulls tens les següents opcions: aclucats, rodons o estrellats")
ulls = input("Quins ulls té? ")
print("\nDel nas tens les següents opcions: aixafat, arromangat o agilenc")
nas = input("Quin tipus de nas té? ")
print("\nDe la boca tens les següents opcions: normal, bigoti o dents_sortides")
boca = input("Quin tipus de boca té? ")

if cabell.lower() == "arrissats":
    print(f"\n{CABELL_ARRISSAT}")
elif cabell.lower() == "llisos":
    print(f"\n{CABELL_LLIS}")
elif cabell.lower() == "pentinats":
    print(f"\n{CABELL_PENTINAT}")
else:
    print("\ncabell erroni")

if ulls.lower() == "aclucats":
    print(f"{ULLS_ACLUCATS}")
elif ulls.lower() == "rodons":
    print(f"{ULLS_RODONS}")
elif ulls.lower() == "estrellats":
    print(f"{ULLS_ESTRELLATS}")
else:
    print("ulls erronis")

if nas.lower() == "aixafat":
    print(f"{NAS_AIXAFAT}")
elif nas.lower() == "arromangat":
    print(f"{NAS_ARROMANGAT}")
elif nas.lower() == "agilenc":
    print(f"{NAS_AGILENC}")
else:
    print("nas erroni")

if boca.lower() == "normal":
    print(f"{BOCA_NORMAL}")
elif boca.lower() == "bigoti":
    print(f"{BOCA_BIGOTI}")
elif boca.lower() == "dents_sortides":
    print(f"{BOCA_DENTS_SORTIDES}")
else:
    print("boca erronia")

print("\nPROGRAMA FINALITZAT")
