"""

   Alex Modesto Mena

   26/10/2021

   ASIXc1B M03 UF1 A2

   Implementa un programa que demani a l'usuari un nombre per pantalla i indiqui si és parell o senar.

"""

nombre1 = float(input("Nombre 1? "))

if nombre1 % 2 == 0:
    print(f"El nombre {nombre1} és parell")
else:
    print(f"El nombre {nombre1} és senar")
