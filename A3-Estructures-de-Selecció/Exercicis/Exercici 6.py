"""

   Alex Modesto Mena

   26/10/2021

   ASIXc1B M03 UF1 A2

   Realitza un programa que demani una cadena per teclat, que comprovi si està escrita en majúscules o no i aleshores
   mostri un missatge en conseqüència.

"""

cadena = input("Frase? ")

if cadena != cadena.upper():
    print("\nLa frase no esta escrita en majúscules")
else:
    print("\nLa frase esta escrita en majúscules")

print("\nPROGRAMA FINALITZAT")
