"""

   Alex Modesto Mena

   26/10/2021

   ASIXc1B M03 UF1 A2

   Escriu un programa que demani dos nombres per teclat i mostri la seva divisió. El programa ha de comprovar si el
   segon nombre és igual a zero. Si ho és en lloc de realitzar i mostrar el resultat de la divisió ha de mostrar un
   missatge d'error per pantalla indicant que la divisió no es pot realitzar, ja que el segon nombre és zero.

"""

nombre1 = float(input("Numero 1? "))
nombre2 = float(input("Numero 2? "))

divisio = nombre1 / nombre2

if nombre2 == 0:
    print("La divisió no es pot realitzar ja que el segón nombre és 0! ")
else:
    divisio = nombre1 / nombre2
    print(f"El resultat de la divisió és {divisio}")
