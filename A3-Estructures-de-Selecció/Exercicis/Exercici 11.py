"""

   Àlex Modesto Mena

   2/11/2021

   ASIXc1B M03 UF1 A2

   Escriu un programa que llegeixi per pantalla la longitud dels tres costats d'un triangle. El programa ha de ser capaç
   de determinar de quin tipus de triangle es tracta.

    Tingueu en compte que:
    Si es compleix el teorema de Pitàgores, llavors és triangle rectangle
    Si només dos costats del triangle són iguals llavors aquest és isòsceles
    Si els tres costats són iguals llavors és un equilàter
    Si no es compleix cap de les condicions anteriors, és un triangle escalè.

"""

import math

a = float(input("Costat a? "))
b = float(input("Costat b? "))
c = float(input("Costat c? "))

pitagores1 = math.sqrt((b**2) + (c**2))
pitagores2 = math.sqrt((a**2) + (c**2))
pitagores3 = math.sqrt((a**2) + (b**2))

if a == b == c:
    print("EQUILATER")
if a == b != c or a == c != b or b == c != a:
    print("ISÒSCELES")
if pitagores1 or pitagores2 or pitagores3:
    print("PITÀGORES")
else:
    print("ERROR EN LES DADES INTRODUÏDES")

print("\nPROGRAMA FINALITZAT")
