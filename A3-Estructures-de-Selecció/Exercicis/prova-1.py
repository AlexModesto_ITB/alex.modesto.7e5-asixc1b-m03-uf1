"""
    Àlex Modesto Mena
    06/10/2021
    ASIXc1B M03 UF1 A2

"""

nota = float(input("Dime tu nota:"))

if nota >=1 and nota < 5:
    print("Suspenso")
elif nota >= 5 and nota < 6:
    print("Suficiente")
elif nota >= 6 and nota < 7:
    print("Bien")
elif nota >= 7 and nota <9:
    print("Notable")
elif nota >=9 and nota <= 10:
    print("Sobresaliente")
else:
    print("Nota incorrecta")

print("Programa terminado")
