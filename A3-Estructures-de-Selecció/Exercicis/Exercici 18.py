"""

   Àlex Modesto Mena

   2/11/2021

   ASIXc1B M03 UF1 A2

   Escriu un programa que llegeixi per pantalla la longitud dels tres costats d'un triangle. El programa ha de ser capaç
   de determinar de quin tipus de triangle es tracta.

    Tingueu en compte que:
    Si es compleix el teorema de Pitàgores, llavors és triangle rectangle
    Si només dos costats del triangle són iguals llavors aquest és isòsceles
    Si els tres costats són iguals llavors és un equilàter
    Si no es compleix cap de les condicions anteriors, és un triangle escalè.

"""

idioma = input("ENGLISH, CATALÀ, CASTELLANO? ")

if idioma.lower() == "english":
    dia = int(input("Select a day between 1 and 7"))
    if dia == 1:
        print()
    elif dia == 2:
        print()
    elif dia == 3:
        print()
    elif dia == 4:
        print()
    elif dia == 5:
        print()
    elif dia == 6:
        print()
    elif dia == 7:
        print()
    else:
        print("ERROR")
elif idioma.lower() == "catala":
    print("b")
elif idioma.lower() == "castellano":
    print("c")

