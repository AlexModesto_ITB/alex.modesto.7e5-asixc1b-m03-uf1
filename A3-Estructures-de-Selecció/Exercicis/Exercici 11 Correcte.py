"""

   Àlex Modesto Mena

   2/11/2021

   ASIXc1B M03 UF1 A2

   Escriu un programa que llegeixi per pantalla la longitud dels tres costats d'un triangle. El programa ha de ser capaç
   de determinar de quin tipus de triangle es tracta.

    Tingueu en compte que:
    Si es compleix el teorema de Pitàgores, llavors és triangle rectangle
    Si només dos costats del triangle són iguals llavors aquest és isòsceles
    Si els tres costats són iguals llavors és un equilàter
    Si no es compleix cap de les condicions anteriors, és un triangle escalè.

"""

costat_a = float(input("Introdueix la longitud del costat a: "))
costat_b = float(input("Introdueix la longitud del costat b: "))
costat_c = float(input("Introdueix la longitud del costat c: "))

trianglellista = []
trianglellista.append(costat_a)
trianglellista.append(costat_b)
trianglellista.append(costat_c)
hipotenusa = max(trianglellista)
catet_a = 0
catet_b = 0

if costat_a == hipotenusa:
    catet_a = costat_b
    catet_b = costat_c
elif costat_b == hipotenusa:
    catet_a = costat_a
    catet_b = costat_c
elif costat_c == hipotenusa:
    catet_a = costat_b
    catet_b = costat_a

if costat_a == costat_b != costat_c or costat_a == costat_c != costat_b or costat_b == costat_c != costat_a:
    print("\nES UN TRIANGLE ISOSCELES")
    if catet_a**2 + catet_b**2 <= round(hipotenusa**2):
        print(" i RECTANGLE")
elif costat_a == costat_b == costat_c:
    print("\nES UN TRIANGLE EQUILATER!")
    if catet_a**2 + catet_b**2 <= round(hipotenusa**2):
        print(" i RECTANGLE")
else:
    print("\n ES UN TRIANGLE ESCALE")

print("\nPROGRAMA FINALITZAT")
