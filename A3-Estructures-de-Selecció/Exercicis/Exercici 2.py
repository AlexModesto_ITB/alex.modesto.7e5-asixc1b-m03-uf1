"""

   Alex Modesto Mena

   26/10/2021

   ASIXc1B M03 UF1 A2

   Crea un programa que llegeixi un nombre per pantalla i mostri en pantalla un missatge que digui si és positiu,
   negatiu o igual a zero.

"""

nombre1 = float(input("Nombre 1? "))

if nombre1 < 0:
    print(f"El número {nombre1} és negatiu! ")
elif nombre1 == 0:
    print(f"El número {nombre1} és igual a 0!")
else:
    print(f"El número {nombre1} és positiu")
