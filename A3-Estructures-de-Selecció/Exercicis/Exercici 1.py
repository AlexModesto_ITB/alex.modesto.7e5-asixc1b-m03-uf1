"""

   Alex Modesto Mena

   26/10/2021

   ASIXc1B M03 UF1 A2

   Escriu un programa que demani per teclat dos nombres i mostri un missatge que digui si el primer és més gran que el
   segon o bé que no ho és.

"""

nombre1 = float(input("Nombre 1? "))
nombre2 = float(input("Nombre 2? "))

if nombre1 > nombre2:
    print("El nombre 1 és més gran que el nombre 2! ")
elif nombre1 == nombre2:
    print("El nombre 1 és igual al nombre 2! ")
else:
    print("El nombre 1 és més petit que el nombre 2!")
