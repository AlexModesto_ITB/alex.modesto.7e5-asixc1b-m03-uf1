"""
    Àlex Modesto Mena
    26/01/2021
    ASIXc1B M03 UF1 A5
    Indica si una paraula és palíndrom.
"""
paraula = 0
while paraula != "q":
    paraula = input("\nDigues una paraula? (Per sortir escriu q): ")
    if paraula.lower() != paraula.lower()[::-1]:
        print(f"{paraula} no és un palíndrom!")
    else:
        print(f"Correcte! {paraula} és un palíndrom!")
print("\nPrograma Finalitzat")
