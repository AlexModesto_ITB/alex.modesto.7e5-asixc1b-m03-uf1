"""
    Àlex Modesto Mena
    18/01/2021
    ASIXc1B M03 UF1 A5
    Programa que generi una llista de 100 nombres aleatoris entre 1 i 50. Obtenir la mitja dels nombres que es troben a les posicions parelles i la mitja del nombre de les  posicions senars.
"""
import random

llista = []
countp = 0
counts = 0
count = 0
parells = 0
senars = 0

for a in range(1, 101):
    llista.append(random.randint(1, 50))

for i in llista:
    count += 1
    if count % 2 == 0:
        parells += i
        countp += 1
    else:
        senars += i
        counts += 1

print(llista)
print(f"La mitja dels nombres parells es: {round(parells / countp, 2)}")
print(f"La mitja dels nombres senars es: {round(senars / counts, 2)}")

print("\nPROGRAMA FINALITZAT")
