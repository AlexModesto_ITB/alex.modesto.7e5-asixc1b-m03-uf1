"""
    Àlex Modesto Mena
    18/01/2021
    ASIXc1B M03 UF1 A5
    Programa de traducció d’insults. Crear un array de dues dimensions amb els insults en català i afegir la traducció en castellà, anglès i klingon. El programa, demanarà a l’usuari que escrigui per teclat un insult i el mostrarà traduït a castellà, anglès  i klingon.
"""

llista_2D = [["Llanut", "Ignorante", "Ignorant", "jIv"],
             ["Cul d’olla", "Inútil", "Useless", "tu'HomI'raH"],
             ["Titella", "Manipulable", "Manipulable", "tlhuH"],
             ["Mitja merda", "Cobarde", "Coward", "bIHnuch"],
             ["Carallot", "Gilipollas", "Douchebag", "QEL"],
             ["Malparit", "Bastardo", "Bastard", "ROD"],
             ["Llepaculs", "Lameculos", "Toady", "DAHJAJ"],
             ["Beneit", "Bobalicon", "Goofy", "JAH"]]

idiomes = ["Català", "Castellà", "Anglès", "Klingon"]

print("1. Llanut \n2. Cul d'olla \n3. Titella \n4. Mitja merda \n5. Carallot \n6. Malparit \n7. Llepaculs \n8. Beneit")
print("ATENCIÓ!!!! HO HAS D'ESCRIURE IGUAL!")
paraula = str(input("Paraula en català: "))

for fila in range(len(llista_2D)):
    for columna in range(len(idiomes)):
        if paraula == llista_2D[fila][columna]:
            for i in range(4):
                print(f"La traducció al {idiomes[i]} es {llista_2D[fila][columna+i]}")
