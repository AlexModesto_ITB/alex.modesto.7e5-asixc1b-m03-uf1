"""
    Àlex Modesto Mena
    26/01/2021
    ASIXc1B M03 UF1 A5
    Mostrar per pantalla cap i cua si la llista de N valors introduïts per l'usuari són cap i cua (llegits en ordre invers és la mateixa llista).
"""

quantitatnumeros = int(input("Quants nombres introduïras? "))
numeros = []

for numero in range (quantitatnumeros):
    pregunta = input("Quins numeros introdueixes? ")
    numeros.append(pregunta)

if numeros != numeros[::-1]:
    print("\nAquest ordre no és un cap i cua")
else:
    print("\nAquest nombres si formen un cap i cua")
print(numeros)
print("\nPrograma Finalitzat")



"""
Método Umar

usuari = input("Numeros separats per un espai: ")
numeros = usuari.split(" ")

if numeros == numeros[::-1]:
    print(f"\nCap i cua")
else:
    print(f"\nNO hay cap i cua")
"""
