"""
UMAR MOHAMMAD RIAZ
05/10/2021
ASIXc 1B

Donat un nombre de dues xifres, escriu el programa que permet mostrar el nombre de manera "invertida".
Per exemple, si l'usuari introdueix 14, el programa hauria de mostrar 41.
"""

numero = input("Introduiex un numero: ")

inversa = (numero[::-1])

print(inversa)
