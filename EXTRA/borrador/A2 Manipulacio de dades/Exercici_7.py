"""
UMAR MOHAMMAD RIAZ
05/10/2021
ASIXc 1B

Implementa un programa que llegeixi del teclat una quantitat de minuts i després mostri en pantalla a quantes
hores i minuts correspon aquesta quantitat.

Per exemple: 1350 minuts són 22 hores i 30 minuts.

"""


temps = float(input("Escriu els minuts: "))
mins = temps % 60
hora = (temps // 60)

print()
print("Son", int(hora), "hores i", int(mins), "minuts.")
