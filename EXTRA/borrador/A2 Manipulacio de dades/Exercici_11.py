"""
UMAR MOHAMMAD RIAZ
05/10/2021
ASIXc 1B

Realitza un programa que mostri la "distància" entre dos nombres entrats per teclat.

Nota: la "distància entre dos nombres" és el valor absolut de la seva diferència.

Per exemple: distància (5,3) = / 5 - 3 / = 2, distància (15,22) = / 15 - 22 / = 7

"""

distancia = input("Introduiex la distancia com [a,b] ")

distancia_lista = distancia.split(",")

num_1 = int(distancia_lista.pop(0))
num_2 = int(distancia_lista.pop(0))

distancia_final = abs(num_1 - num_2)

print()
print(f"la distancia es: {distancia_final}")






