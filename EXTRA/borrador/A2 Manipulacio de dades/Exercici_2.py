
"""
UMAR MOHAMMAD RIAZ
05/10/2021
ASIXc 1B

Donada la base i altura d'un rectangle, escriure el programa que calcula i mostra el seu perímetre i àrea.

"""

base = float(input("Base del rectangle: "))
height = float(input("Altura del rectangle: "))

perimeter = float(base + height+ base + height)
area = float(base * height)

print()
print(f"El perimetre es {perimeter}")
print(f"El area es {area}")
