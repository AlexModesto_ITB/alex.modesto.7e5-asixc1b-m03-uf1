"""Indica si una paraula és palíndrom.

Exemples de palíndroms:
cec, cuc, gag, mínim, nan, nen, pipiripip…"""

paraula = input("Digues una paraula: ")

if paraula.upper() == paraula[::-1].upper():
    print(f"\n {paraula} es palindrom")
else:
    print(f"\n {paraula} NO es palindrom")
