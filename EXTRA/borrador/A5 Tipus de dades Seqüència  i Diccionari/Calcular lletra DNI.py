

lletres = ["T", "R", "W", "A", "G", "M", "Y", "F", "P", "D", "X", "B", "N", "J",
          "Z", "S", "Q", "V", "H", "L", "C", "K", "E"]

DNI = int(input("Numeros del teu DNI: "))

if len(str(DNI)) == 8:
    resta = DNI % 23
    lletra = lletres[resta]
    print(f"\nEl teu DNI és {DNI}{lletra}")
else:
    print("\nEl DNI té 8 numeros!")
