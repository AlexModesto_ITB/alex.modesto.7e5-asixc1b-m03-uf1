"""
Donada una paraula i un valor indica quina lletra hi ha a la posició indicada
"""


usuari = input("Paraula i posicio: ")

usuarilista = usuari.split(" ")

paraula = usuarilista[0]
posicio = int(usuarilista[1])

if posicio < len(paraula) and posicio >= -len(paraula):
    print(paraula[posicio])
else:
    print("\nFora de rang")



