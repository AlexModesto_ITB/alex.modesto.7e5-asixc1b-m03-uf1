"""
UMAR MOHAMMAD RIAZ
21/12/2021
ASIXc 1B

Realitzar un programa que inicialitzi una llista amb 10 valors aleatoris (de l'1 al 10) i posteriorment mostri
en pantalla cada nombre de la llista juntament amb el seu quadrat i el seu cub."""

import random

llistanum = []

for i in range(1, 11):
    llistanum.append(random.randint(1, 10))

for num in llistanum:
    print("-------------")
    print(num, "|", num ** 2, "|", num ** 3)
