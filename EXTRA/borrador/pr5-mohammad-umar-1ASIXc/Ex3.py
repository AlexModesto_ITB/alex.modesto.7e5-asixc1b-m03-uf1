"""
Umar Mohammad Riaz
1r ASIXc
18/01/2022

3. Programa de traducció d’insults. Crear un array de dues dimensions amb els insults en català i
afegir la traducció en castellà, anglès i klingon
El programa, demanarà a l’usuari que escrigui per teclat un insult i el mostrarà traduït a castellà,
anglès  i klingon.
"""

insults = [["MOCÓS", "MOCOSO", "BRAT", "QEV"],
           ["CARALLOT", "GILIPOLLAS", "DOUCHEBAG", "QEL"],
           ["MALPARIT", "BASTARDO", "BASTARD", "ROD"],
           ["LLEPACULS", "LAMECULOS", "TOADY", "DAHJAJ"],
           ["LLANUT", "LANUDO", "WOOLLY", "QIM"],
           ["BENEIT", "BOBALICÓN", "GOOFY", "JAH"],
           ["LLANUT", "IGNORANTE", "IGNORANT", "JIV"],
           ["CUL D’OLLA", "INÚTIL", "USELESS", "TU'HOMI'RAH"],
           ["TITELLA", "MANIPULABLE", "MANIPULABLE", "TLHUH"],
           ["MITJA MERDA", "COBARDE", "COWARD", "BIHNUCH"]]


idiomes = ["CATALÀ", "CASTELLANO", "ENGLISH", "KILNGON"]

usuari = input("Digues un insult en \ncatalà, castellano, english o klingon: ")

contador = 0
trobat = False

while not trobat and contador != len(insults):
    if usuari.upper() in insults[contador]:
        print()
        for idioma, insult in zip(idiomes, insults[contador]):
            print(idioma + ": " + insult.capitalize())
            trobat = True
    else:
        contador += 1

if not trobat:
    print(f"\n '{usuari}' no está en el diccionari!")

print("\nPROGRAMA FINALITZAT!")
