"""
UMAR MOHAMMAD RIAZ
1B ASIXc
14/12/2021

1. SumPositiveValues
"""

variables = int(input("Quantes variables vols? "))
producte = 0

for x in range(variables):
    numero = float(input("Introdueix un número: "))
    if numero >= 0:
        producte += numero

print("\nLa suma dels positius es", producte)

