"""
UMAR MOHAMMAD RIAZ
1B ASIXc
14/12/2021

3. Matrix
"""

BLANC = " B "
NEGRE = " N "
MOVIMENT = " * "

mida = int(input("Mida del tauler: "))
fila = int(input(f"\nFILA: "))
columna = int(input(f"COLUMNA: "))

print("\nTauler:")
for x in range(mida):
    for y in range(mida):
        if x == fila or y == columna:
            print(MOVIMENT, end="")
        elif (x + y) % 2 == 0:
            print(BLANC, end="")
        else:
            print(NEGRE, end="")
    print()
