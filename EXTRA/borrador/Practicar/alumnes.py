"""Volem desar els noms i les edats dels alumnes de curs. Feu un programa que introdueixi el nom i
l'edat de cada alumne. El procés de lectura de dades acabarà quan s'introdueixi com a nom un asterisc (*)
En finalitzar es mostrarà les dades següents:
Els alumnes majors d’edat
Els alumnes més vells: per fer això caldrà primer que obtingueu quina és l’edat més gran que us han
entrat i aleshores mostrar els alumnes que tinguin aquesta edat (pot haver-hi més d’un)
"""

alumnes = []
edats = []
nom = ""
while nom != "*":
    nom = input("\nNom del alumne (* per sortir): ")
    if nom != "*":
        edat = int(input(f"Edat de {nom}: "))
        alumnes.append(nom)
        edats.append(edat)

vells_edat = []
vells_nom = []

print("\nMajors d'edat:")
for nom, edat in zip(alumnes, edats):
    if edat >= 18:
        print(nom.capitalize(), edat)







