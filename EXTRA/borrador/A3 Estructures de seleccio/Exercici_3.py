"""
UMAR MOHAMMAD RIAZ
20/10/2021
ASIXc 1B

Implementa un programa que demani a l'usuari un nombre per pantalla i indiqui si és parell o senar.
"""

numero = float(input("Entra un numero: "))

if numero % 2 == 0:
    print("PAR")
else:
    print("IMPAR")

print("\nPrograma Finalitzat")

