"""
    Umar Mohammad
    27/10/2021
    ASIXcB M03 UF1 A3

    Un grup d'investigadors ens ha demanat un programa per generar retrats robots.
    L'usuari ha d'introduir el tipus de cabells, ulls, nas i boca i s'imprimeix per pantalla un dibuix de com és el sospitós.
    Els cabells poden ser arrissats @@@@@, llisos VVVVV o pentinats XXXXX
    Els ulls poden ser aclucats .-.-., rodons .o-o. o estrellats .*-*.
    El nas pot ser aixafat ..0.., arromangat ..C.. o agilenc ..V..
    La boca pot ser normal .===. , bigoti  .∼∼∼.  o dents-sortides  .www.
"""
#CABELL
ARRISSATS = "@@@@@"
LLISOS = "VVVVV"
PENTITATS = "XXXXX"

#ULLS
ACLUCATS = ".-.-."
RODONS = ".o-o."
ESTRELLATS = ".*-*."

#NAS
AIXAFAT = "..0.."
ARROMANGAT = "..C.."
AGILENC = "..V.."

#BOCA
NORMAL = ".===."
BIGOTI = ".~~~."
DENTS_SORTIDES = ".WWW."

cabell = input("Com tens el cabell? Arrissats, llisos o pentinats? \n")
ulls = input("\nCom tens els ulls? Aclucats, rodons o estrellats? \n")
nas = input("\nCom tens el nas? Aixafat, arromangat o agilenc? \n")
boca = input("\nCom tens la boca? Normal, bigoti o dens sortides? \n")

if cabell.lower() == "arrissats":
    print("\n" + str(ARRISSATS))
elif cabell.lower() == "llisos":
    print("\n" + str(LLISOS))
elif cabell.lower() == "pentinats":
    print("\n" + str(PENTITATS))
else:
    print("ERROR!")

if ulls.lower() == "aclucats":
    print(ACLUCATS)
elif ulls.lower() == "rodons":
    print(RODONS)
elif ulls.lower() == "estrellats":
    print(ESTRELLATS)
else:
    print("ERROR!")

if nas.lower() == "aixafat":
    print(AIXAFAT)
elif nas.lower() == "arromangat":
    print(ARROMANGAT)
elif nas.lower() == "agilenc":
    print(AGILENC)
else:
    print("ERROR!")

if boca.lower() == "normal":
    print(NORMAL)
elif boca.lower() == "bigoti":
    print(BIGOTI)
elif boca.lower() == "dents-sortides" or boca.lower() == "dents sortides":
    print(DENTS_SORTIDES)
else:
    print("ERROR!")
