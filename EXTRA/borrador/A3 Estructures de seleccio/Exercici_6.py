"""
UMAR MOHAMMAD RIAZ
20/10/2021
ASIXc 1B

Realitza un programa que demani una cadena per teclat, que comprovi si està escrita en majúscules o no i
aleshores mostri un missatge en conseqüència.

"""

cadena = input("Frase: ")

if cadena != cadena.upper():
    print("NO esta tot en MAJUSCULES!")
else:
    print("CORRECTE")

print("\nPROGRAMA FINALITZAT")


