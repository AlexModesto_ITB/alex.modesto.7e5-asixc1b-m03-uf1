"""
UMAR MOHAMMAD RIAZ
20/10/2021
ASIXc 1B

Escriu un programa que demani per teclat dos nombres i mostri un missatge que digui si el primer és més gran que el
segon o bé que no ho és.
"""

numeros = input("Introduiex dos numeros separats per un espai: ")

numeroslista = numeros.split(" ")

a = float(numeroslista.pop(0))
b = float(numeroslista.pop(0))

if a > b:
    print(f"\n{a} es major que {b}")
else:
    print(f"\n{a} NO es major que {b}")

print("\nPrograma finalitzat")
