"""

  Umar Mohammad RIaz
  09/11/2021
  ASIXc1A M03 UF1

  Programa entre en bucle fins que es complexi una condició.
  Per exemple: Sortir? (s/n)

"""

cont = 0
sortir = False

while not sortir:
   cont = cont + 1
   print(cont)
   opció = input("Sortir? (s/n): ")
   if opció.lower() == "s":
       sortir = True


print("Has sortit ... ets lliure ;-)")
