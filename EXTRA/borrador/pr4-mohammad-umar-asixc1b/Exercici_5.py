"""
UMAR MOHAMMAD RIAZ
16/11/2021
ASIXc 1B

Programa que realitza la multiplicació, de dos números sencers,  mitjançant sumes
"""
multiplicador = int(input("Introdueix el multiplicador: "))
numeromultiplicat = int(input("Introdueix un numero: "))

RESULTAT = 0

for numero in range(abs(multiplicador)):
    RESULTAT = RESULTAT + abs(numeromultiplicat)

if multiplicador < 0 and numeromultiplicat > 0 or multiplicador > 0 and numeromultiplicat < 0:
    print(f"\n-{RESULTAT}")
elif multiplicador < 0 and numeromultiplicat < 0:
    print(f"\n{RESULTAT}")
else:
    print(f"\n{RESULTAT}")
