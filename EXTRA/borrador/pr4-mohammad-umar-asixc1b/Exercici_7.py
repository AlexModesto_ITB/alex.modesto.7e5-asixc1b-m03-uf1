"""
UMAR MOHAMMAD RIAZ
16/11/2021
ASIXc 1B

Programa que mostra els nombres primers que hi ha entre el 2 i un número superior introduït per l'usuari.
"""
numero = int(input("Introdueix un numero: "))

comptador = 0

for n in range(numero):
    for d in range(1, n + 1):
        if n % d == 0:
            comptador += 1
    if comptador == 2:
        print(n)
    comptador = 0
