"""
UMAR MOHAMMAD RIAZ
16/11/2021
ASIXc 1B

Programa que demana un número a l'usuari i suma els seus dígits.
"""

numero = input("Escriu un numero: ")
suma = 0

for digit in numero:
    suma = suma + int(digit)

print(suma)
