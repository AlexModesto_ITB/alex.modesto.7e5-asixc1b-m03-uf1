"""

   Alex Modesto Mena

   29/09/2021

   ASIXc1B M03 UF1 A1

   Math operations examples

"""

x = 5

y = 3

print("x = 5")

print("y = 3")

print("\n")



#Addition

addition = x + y

print("Addition: x + y = " + str(addition))



#Subtraction

subtraction = x - y

print("Subtraction: x + y = " + str(subtraction))



#Multiplication

multiplication = x * y

print("Multiplication: x * y = " + str(multiplication))



#Division

division1 = x / y

print("Division: x / y = " + str(division1))



#Division (round down/ floor)

division2 = x // y

print("Division: x / y = " + str(division2))



#Modulus (remainder)

modulus = x % y

print("Modulus: x % y = " + str(modulus))



#Power

power = x ** y

print("Power: x ** y = " + str(power))
