"""

   Àlex Modesto Mena

   30/11/2021

   ASIXc1B M03 UF1 A4

   Mostrar per pantalla un quadrat de mida N (la triarà l'usuari)
   Cal "printar"  #  a les vores. És a dir, dalt, baix dreta i esquerra
   Cal fer servir estructures de repetició, per fer servir la mínima quantitat possible de sentències print input

"""

mida = int(input("Quina mida? "))

for fila in range (mida):
    for columna in range (mida):
        if fila == 0 or fila == mida-1 or columna == 0 or columna == mida-1:
            print("0  ", end="")
        else:
            print("x  ", end="")
    print()
