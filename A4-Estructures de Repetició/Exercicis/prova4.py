"""#Using FOR

for i in range(1, 10):
   for j in range(1, 10):
       print("%d x %d = %d"%(i,j,i*j))
   print("--------------")
"""

#Using WHILE

contI=1

contJ=1

while contI<=10:
    contJ = 0
    while contJ <= 10:
        print("%d x %d = %d"%(contI,contJ,contI*contJ))
        contJ+=1
    print("--------------")
    contI = contI + 1
