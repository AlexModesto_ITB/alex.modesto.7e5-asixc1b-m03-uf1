"""

   Àlex Modesto Mena

   30/11/2021

   ASIXc1B M03 UF1 A4
   Escriu un programa que mostri el següent menú d’opcions:
   -Literatura
   -Cinema
   -Música
   -Videojocs
   -Sortir
   Si l’usuari tria una opció de l’1 al 4, el programa ha de mostrar  uns quants suggeriments de títols relacionats amb el tema escollit. Si l’usuari tria una opció no contemplada, el programa ha de mostrar un missatge d’error. En tot cas, el programa tornarà a mostrar el menú d’opcions, tret que l’usuari triï l’opció 5: en aquest cas, el programa mostrarà un missatge de comiat i acabarà.

"""

print(" 1.Literatura")
print(" 2.Cinema")
print(" 3.Música")
print(" 4.Videojocs")
print(" 5.Sortir")

opcio = int(input("\nTria una opció (1-5): "))

while opcio != 5:
    if opcio == 1:
        print("Literatura")
    elif opcio == 2:
        print("Cinema")
    elif opcio == 3:
        print("Música")
    elif opcio == 4:
        print("Videojocs")
    else:
        print("Error en les dades introduïdes!!")
    opcio = int(input("\nTria una opció (1-5): "))
print("Has sortit!")
