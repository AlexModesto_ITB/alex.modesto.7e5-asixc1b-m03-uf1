"""
Programa que pinta per pantalla una matriu de 8x8 (fileres x columnes) amb zeros a cada posició, a exepció dels diagonals.
"""
#CREU X

blanc = "1 "
negre = "0 "

num = int(input("De cuanto quieres el tablero: "))
for i in range(num):
    for j in range(num):
        if i == j or (i + j ==num-1):
            print(blanc,end="")

        else:
            print(negre,end="")
    print()




"""
CRUZ+

UNOS = "1 "
ZEROS = "0 "
import math
num = int(input("De cuanto quieres el tablero: "))
for i in range(num):
    for j in range(num):
        if i == round((num/2),0) or j == round((num/2),0):
            print(UNOS,end="")

        else:
            print(ZEROS,end="")
    print()
"""
