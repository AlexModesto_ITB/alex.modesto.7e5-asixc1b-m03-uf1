"""

   Àlex Modesto Mena

   23/11/2021

   ASIXc1B M03 UF1 A4

   Crea una aplicació que permet endevinar un número. L'aplicació genera un nombre “aleatori” de l'1 al 100.
   A continuació, l’aplicació va demanant números i va responent si el nombre a endevinar és més gran o més petit que la
   introduït, a més dels intents que et queden (tens 10 intents per encertar-lo). El programa acaba quan s'encerta el
   número (a més et diu quants intents has necessitat per encertar-lo), si s'arriba al límit d'intents, l’aplicació et
   mostra el número que havia generat.

"""

import random

numero = random.randint(1, 100)

INTENTS = 10
print(numero)
while INTENTS != 0:
    INTENTS -= 1
    usuari = int(input("\nDigues un numero entre 1 i 100: "))
    if usuari == numero:
        print("Has encertat!!")
    elif usuari > numero:
        print("El numero és més petit")
    else:
        print("El numero és més gran")
    print(f"Et queden {INTENTS} intents!")
if
