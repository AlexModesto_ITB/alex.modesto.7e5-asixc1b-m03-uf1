"""

   Àlex Modesto Mena

   1/12/2021

   ASIXc1B M03 UF1 A4

   S'imprimeix una piràmide d'altura N de # centrada

"""

base = int(input("Quants nivells ha de tenir? "))
for nivell in range (-1, base, +2):
    print(" " * nivell+"#"*nivell)
