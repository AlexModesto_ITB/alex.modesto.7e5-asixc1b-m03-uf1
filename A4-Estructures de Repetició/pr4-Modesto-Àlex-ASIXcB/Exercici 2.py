"""

   Àlex Modesto Mena

   16/11/2021

   ASIXc1B M03 UF1 A4

   Programa que mostra per pantalla tots els nombres parells menors que 20.

"""


for i in range (20):
    if i % 2==0:
        print(i)


