"""

    Àlex Modesto Mena
    19/10/2021  ASIXcB
    Part Practica 1 Exercici 2
    Crear un programa que sigui capaç de calcular la nota final de la UF1 del mòdul 03 del Cicle de Grau Superior
    d'Administració de Sistemes Informàtics en Xarxa (CFGS ASIX). El programa només haurà de fer el càlcul per a 3
    estudiants, i mostra'ls per pantalla. Les notes de cadascun dels estudiants s'haurà de demanar per pantalla.
    La fórmula de la UF1 és: QUF1 = 0,30*RA1 + 0,70*RA2. Els pes de cada RA's està fitxat a l'inici de curs per a no
    ser modificat. La nota final de les UF's es calcula amb decimals.

"""
#Aqui demanarem les notes del estudiants
print("Comencem per el primer estudiant:")
RA1_estudiant1 = float(input("Quina és la nota de la RA1? "))
RA2_estudiant1 = float(input("Quina és la nota de la RA2? "))

print("Ara anem pel segon estudiant:")
RA1_estudiant2 = float(input("Quina és la nota de la RA1? "))
RA2_estudiant2 = float(input("Quina és la nota de la RA2? "))

print("I ara el tercer estudiant:")
RA1_estudiant3 = float(input("Quina és la nota de la RA1? "))
RA2_estudiant3 = float(input("Quina és la nota de la RA2? "))

#Aqui fem els calculs de les notes dels estudiants
nota_estudiant1 = ((RA1_estudiant1 * 0.3) + (RA2_estudiant1 * 0.7))
nota_estudiant2 = ((RA1_estudiant2 * 0.3) + (RA2_estudiant2 * 0.7))
nota_estudiant3 = ((RA1_estudiant3 * 0.3) + (RA2_estudiant3 * 0.7))

#En aquest últim pas fem un print per a donar la solució a les notes arrodonides a 2 decimals
print(f"La nota final de la UF1 del estudiant 1 és de {round(nota_estudiant1,2)} punts")
print(f"La nota final de la UF1 del estudiant 2 és de {round(nota_estudiant2,2)} punts")
print(f"La nota final de la UF1 del estudiant 3 és de {round(nota_estudiant3,2)} punts")
