"""

    Àlex Modesto Mena
    19/10/2021  ASIXcB
    Part Practica 1 Exercici 1
    Hem de travessar un camp d'un extrem (superior-esquerre) a un altre (inferior-dret) i volem saber quanta
    distància haurem de recórrer. Només tenim la informació de l'amplada i l'alçada. Demana a l'usuari l'amplada i
    l'alçada d'un camp i mostra la llargada que fa la diagonal, d’un extrem a l’altre.

"""
#Fem un import math per a poder carregar l'arrel mes tard
import math

#Demanem l'altura i l'amplada
amplada = float(input("Amplada en metres? "))
altura = float(input("Alçada en metres? "))

#Calculem la diagonal amb la forma ja donada
diagonal = math.sqrt((amplada**2)+(altura**2))

#Donem la solució al problema donant la diagonal arrodonida a 2 decimals
print(f"La diagonal serà de {round(diagonal,2)} metres")
