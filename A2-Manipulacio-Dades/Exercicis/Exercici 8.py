"""

   Alex Modesto Mena

   06/10/2021

   ASIXc1B M03 UF1 A2

   Un venedor percep cada mes un sou base més un 10% de l'import de cada venda realitzada al mes en concepte de
   comissió de vendes.
   Realitza un programa que a partir d'un import de sou base i l'import de tres vendes, calculi i escriu en pantalla:
   - l'import percebut en concepte de comissió de vendes
   - l'import total percebut pel venedor (el salari)
   Nota: els imports de salari base i de les tres vendes seran entrats per teclat.

"""

sou_base =float(input("Introduieix el sou base: "))
vendes =float(input("Introduieix la quantitat de € d'aquest mes: "))

comissio = vendes * 0.1
sou_final = sou_base + comissio

print(f"El sou final és de {round(sou_final,2)} €")

