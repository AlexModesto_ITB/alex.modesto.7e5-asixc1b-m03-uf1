"""

   Alex Modesto Mena

   06/10/2021

   ASIXc1B M03 UF1 A2

   Realitza un programa que demani a l'usuari graus Fahrenheit i posteriorment els mostri com graus Celsius.
   La fórmula per a la conversió és:
   C = (F-32)*5/9sjo

"""

fahrenheit =float(input("Graus Fahrenheit? "))
centrigrads = (fahrenheit-32)*5/9
#print(f"{fahrenheit} graus fahrenheit són {round(centrigrads,0)} graus centígrads")
print("%.1f graus Fahrenheit són %1.f graus centígrads"%(fahrenheit,centrigrads))
