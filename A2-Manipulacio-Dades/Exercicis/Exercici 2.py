"""

   Alex Modesto Mena

   05/10/2021

   ASIXc1B M03 UF1 A2

   Donada la base i altura d'un rectangle, escriure el programa que calcula i mostra el seu perímetre i àrea.

"""

base =float(input("Base? "))
altura =float(input("Altura? "))
area =base * altura

print("Area: "+str(area))


perimetre =base * 2 + altura * 2
print("Perimetre "+str(perimetre))
