"""

   Alex Modesto Mena

   13/10/2021

   ASIXc1B M03 UF1 A2

   Implementa un programa que demani per teclat dos punts del pla i tot seguit calculi i mostri la distància entre ells.
   Notes:
       Un punt del pla es representa amb un parell de números. Per exemple, el punt A es representarà pels números x1 i
       y1, i el punt B pels números x2 i y2
       La fórmula de la distància entre dos punts A i B és:
       distància(A,B) = √( (x2 - x1)2 + (y2 - y1)2)

"""
import math

puntA = (input("Punt A [x1,y1]? "))
puntB = (input("Punt B [x2,y2]? "))

puntA_llista = puntA.split(",")
puntB_llista = puntB.split(",")

x1 = float(puntA_llista.pop(0))
y1 = float(puntA_llista.pop(0))
x2 = float(puntB_llista.pop(0))
y2 = float(puntB_llista.pop(0))

distancia = math.sqrt(pow((x2 - x1),2) + pow((y2 - y1),2))

print(f"La distància entre A i B és de {round(distancia,2)}")



