"""

   Alex Modesto Mena

   13/10/2021

   ASIXc1B M03 UF1 A2

   Escriu un programa que donat l'import d'una compra, apliqui a aquest import un descompte del 15% i el mostri a pantalla.

"""

compra = float(input("Import compra? "))

comissio = compra * 0.15
cost_final = compra - comissio
print(f"El cost final de la compra és de {round(cost_final,2)}€")
