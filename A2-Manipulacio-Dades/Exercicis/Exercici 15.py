"""

   Alex Modesto Mena

   13/10/2021

   ASIXc1B M03 UF1 A2

  Escriu el programa que demani a l'usuari el valor de dues variables A i B, intercanviï els seus valors i finalment els mostri.

"""

variableA = float(input("Variable A? "))
variableB = float(input("Variable B? "))

temp = variableB
variableB = variableA
variableA = temp

print(f"La variable A és {round(variableA,2)}")
print(f"La variable B és {round(variableB,2)}")
