"""

   Alex Modesto Mena

   13/10/2021

   ASIXc1B M03 UF1 A2

   En un institut la nota de l'assignatura de Programació es calcula de la següent manera:

     • 55% de la mitjana de tres qualificacions parcials.
     • 30% de la nota d’una prova final.
     • 15% de la nota d’un treball final.
     Implementa un programa que calculi la nota final a partir dels valors entrats per teclat corresponents a les
     qualificacions parcials i notes de la prova i treball finals.

"""

parcial1 = float(input("La nota del primer parcial? "))
parcial2 = float(input("La nota del segón parcial? "))
parcial3 = float(input("La nota del tercer parcial? "))

prova_final = float(input("La nota de la prova final? "))

treball = float(input("La nota del treball final? "))

mitjana = (parcial1 + parcial2 + parcial3) / 3

notafinal = (mitjana * 0.55) + (prova_final * 0.3) + (treball * 0.15)

print(f"La nota final de l'assignatura de programació és de {round(notafinal,2)} punts")
