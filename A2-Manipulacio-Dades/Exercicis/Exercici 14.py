"""

   Alex Modesto Mena

   06/10/2021

   ASIXc1B M03 UF1 A2
   Donat un nombre de dues xifres, escriu el programa que permet mostrar el nombre de manera "invertida". Per exemple, si l'usuari introdueix 14, el programa hauria de mostrar 41.

"""

nombre = (input("Quin és el nombre: "))

inversa = (nombre[::-1])

print(inversa)
