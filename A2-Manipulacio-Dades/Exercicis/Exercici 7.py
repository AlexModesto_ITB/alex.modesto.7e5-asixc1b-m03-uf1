"""

   Alex Modesto Mena

   06/10/2021

   ASIXc1B M03 UF1 A2

   Implementa un programa que llegeixi del teclat una quantitat de minuts i després mostri en pantalla a quantes hores i
   minuts correspon aquesta quantitat.
   Per exemple: 1350 minuts són 22 hores i 30 minuts.

"""

temps = float(input("Quants minuts? "))

minuts = temps % 60
hores = temps //60

print(f"El resultat són {round(hores)} hores i {round(minuts)} minuts")
