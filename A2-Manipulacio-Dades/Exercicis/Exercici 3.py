"""

   Alex Modesto Mena

   05/10/2021

   ASIXc1B M03 UF1 A2

   Donats els catets a i b d'un triangle rectangle, escriure el codi Python que calcula la seva hipotenusa.
   Recordar que la hipotenusa es calcula de la següent manera (Teorema de Pitàgores):

   c2 = a2 + b2 , on c és la hipotenusa. Per tant  c = √(a2 + b2)

"""
import math

catet_A =float(input("Catet A? "))
catet_B =float(input("Catet B? "))

hipotenusa = math.sqrt(pow(catet_A,2) + pow(catet_B,2))
hipotenusa2 = math.sqrt(catet_A**2 + catet_B**2)

print("El resultat és: "+str(hipotenusa))
print("El resultat és: "+str(hipotenusa2))
