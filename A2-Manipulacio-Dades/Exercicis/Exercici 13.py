"""

   Alex Modesto Mena

   13/10/2021

   ASIXc1B M03 UF1 A2

  Escriu un programa que llegeixi un nombre i tot seguit calculi i mostri l'arrel quadrada i cúbica d'aquest número.
  Desafortunadament, Python3 no té cap funció predefinida que calculi l'arrel cúbica, però sí que pots utilitzar
  l’operador que “fa l’operació inversa” …

"""
import math

nombre = float(input("Quin és el nombre? "))

arrel2 = math.sqrt(nombre)
arrel3 = pow(nombre,(1. / 3.))
#arrel3 = nombre ** (1. / 3.)

print(f"L'arrel quadruada és {round(arrel2,2)}")
print(f"L'arrel cúbica és {round(arrel3,2)}")


