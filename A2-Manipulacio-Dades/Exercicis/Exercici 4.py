"""

   Alex Modesto Mena

   06/10/2021

   ASIXc1B M03 UF1 A2

   Escriu el programa que calcula i mostra en pantalla la suma, resta, divisió i multiplicació de dos nombres entrats
   per l'usuari.

"""

numero1 =float(input("numero1? "))
numero2 =float(input("numero2? "))
suma = numero1 + numero2
print(f"La suma és {suma}")
resta = numero1 - numero2
print(f"La resta és {resta}")
multiplicacio = numero1 * numero2
print(f"La multiplicació és {multiplicacio}")
divisio = numero1 / numero2
print(f"La divisió és {divisio}")
