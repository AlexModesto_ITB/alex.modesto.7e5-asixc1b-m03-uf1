"""

   Alex Modesto Mena

   13/10/2021

   ASIXc1B M03 UF1 A2

   Realitza un programa que mostri la "distància" entre dos nombres entrats per teclat.
   Nota: la "distància entre dos nombres" és el valor absolut de la seva diferència.
   Per exemple: distància (5,3) = / 5 - 3 / = 2, distància (15,22) = / 15 - 22 / = 7

"""

numero1 = float(input("Número 1? "))
numero2 = float(input("Número 2? "))

distancia = abs(numero1 - numero2)


print(f"La distancia entre {round(numero1,2)} i {round(numero2,2)} és {round(distancia,2)}")
