"""

   Alex Modesto Mena

   06/10/2021

   ASIXc1B M03 UF1 A2

   Escriu un programa que demani a l'usuari entrar tres nombres i després escrigui a la pantalla la seva mitjana.

"""

numero1 =float(input("Numero 1? "))
numero2 =float(input("Numero 2? "))
numero3 =float(input("Numero 3? "))

mitjana = (numero1 + numero2 + numero3) / 3

print(f"La mitjana és de {mitjana}")
